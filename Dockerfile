FROM python:latest
MAINTAINER Yamila Moreno yamila.moreno@kaleidos.net
WORKDIR /

COPY requirements.txt /
RUN pip install -r /requirements.txt

COPY src /calculus
WORKDIR /calculus
