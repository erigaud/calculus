from calculus import add, sub

def test_add_ok():
    result = add(1,2)
    assert result == 3

def test_sub_ok():
    result = sub(2,1)
    assert result == 1
